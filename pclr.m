# pclr.m
# 
# Principal Component Linear Regression with selection for significant
# principal components.
#
# Inputs:
#   y       - Nx1 vector of (centered) target values
#   x       - NxM design matrix (centered)
#   q       - Desired false discovery rate / false discovery probability (default=0.1)
#   ridge   - Tikhonov (L2) regularization constant (default=0)
#   method  - Significance cutoff criteria
#               0 = Benjamini-Hochberg FDR (default)
#               1 = Strict probability of false acceptance
#   tol     - Minimum non-zero value of eigenvalues
#
# Outputs:
#   beta_q  - Mx1 Coefficient estimates based on chosen principal components
#   beta    - MxM matrix of coefficient estimates built progressively by
#             including principal components in order of significance.
#   p       - P-values for each principal component, in order of significance
#   k       - Number of principal components included, +1; beta_q = beta(:,k) 
#   df      - Residual degrees of freedom used
#
# Notes:
#   Note that beta(:,1) = zeros(m,1), and will be chosen if no principal
#   components are found to be significant (k=1).
#
# Author(s): T. Galoppo (tjg2107@columbia.edu)
#            C. Kogan (kogan.clark@gmail.com)
#
function [beta_q beta p k df] = pclr(y, x, q=0.1, ridge=0, method=0, tol=eps)
  [n m] = size(x);
  
  # eigendecomposition
  [u v] = eig(x' * x + ridge * eye(m));
  # we need xu to build the estimator trace
  xu = x * u;
  # extract the eigenvalues as vector
  lambda = diag(v);
  # pseudo-inverse of eigenvalues
  lambda_inv = ifelse(lambda > tol, 1 ./ lambda, 0);
  # pinv_v = pinv(v), cleverly  
  pinv_v = diag(lambda_inv);
  
  # this is, in effect, cov(xu,y) / var(principal components)
  beta0 = pinv_v * xu' * y;
  # questionable degree of freedom determination
  df = max(1, n - rank(pinv_v)); 
  # estimate residual squared error
  sigma_sq = sum((xu * beta0 - y) .^ 2) / df;
  # compute stderr of coefficient estimates  
  stderr = sigma_sq .* lambda_inv;
  stderr = ifelse(stderr > tol, sqrt(stderr), 0);
  # compute number of standard errors from zero for each...
  tvals = ifelse(stderr > tol, abs(beta0 ./ stderr), 0); 
  # compute p-values for each principal component
  pvals = 2*(1-tcdf(tvals, df));
  # sort the pvalues; p is sorted list, l is indices
  [p l] = sort(pvals); 
  
  # progressively build coefficient estimates, adding principal components
  # by significance
  beta = zeros(m,m+1);
  for j=1:m
    beta(:,j+1) = beta(:,j) + xu(:,l(j))' * y * lambda_inv(l(j)) * u(:,l(j));
  endfor
  
  # choose cutoff point
  if method == 0
    k = benjamini_hochberg(p,q);
  else
    k = strict_prob(p,q);
  endif
 
  # return selected coefficient vector
  beta_q = beta(:,k);  
endfunction

function n = benjamini_hochberg(p, q)
  m = size(p,1);
  n = m+1;
  for j=1:m
    if p(j) > q * j / m
      n = j;
      break;
    endif
  endfor
endfunction

function n = strict_prob(p, q) 
  m = size(p,1);
  n = m+1;
  prd = 1;
  for j=1:m
    prd = prd * (1 - p(j));
    if prd < (1-q)
      n = j;
      break;
    endif
  endfor
endfunction
